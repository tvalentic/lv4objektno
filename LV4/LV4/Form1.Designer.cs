﻿namespace LV4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewOrders = new System.Windows.Forms.DataGridView();
            this.lblOrders = new System.Windows.Forms.Label();
            this.checkBoxEmployees = new System.Windows.Forms.CheckBox();
            this.checkBoxCustomers = new System.Windows.Forms.CheckBox();
            this.checkBoxShippers = new System.Windows.Forms.CheckBox();
            this.lblEmployees = new System.Windows.Forms.Label();
            this.lblCustomers = new System.Windows.Forms.Label();
            this.lblShippers = new System.Windows.Forms.Label();
            this.cboEmployees = new System.Windows.Forms.ComboBox();
            this.cboCustomers = new System.Windows.Forms.ComboBox();
            this.cboShippers = new System.Windows.Forms.ComboBox();
            this.buttonShow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewOrders
            // 
            this.dataGridViewOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrders.Location = new System.Drawing.Point(30, 43);
            this.dataGridViewOrders.Name = "dataGridViewOrders";
            this.dataGridViewOrders.RowTemplate.Height = 24;
            this.dataGridViewOrders.Size = new System.Drawing.Size(800, 348);
            this.dataGridViewOrders.TabIndex = 1;
            // 
            // lblOrders
            // 
            this.lblOrders.AutoSize = true;
            this.lblOrders.Location = new System.Drawing.Point(30, 20);
            this.lblOrders.Name = "lblOrders";
            this.lblOrders.Size = new System.Drawing.Size(52, 17);
            this.lblOrders.TabIndex = 2;
            this.lblOrders.Text = "Orders";
            // 
            // checkBoxEmployees
            // 
            this.checkBoxEmployees.AutoSize = true;
            this.checkBoxEmployees.Location = new System.Drawing.Point(208, 438);
            this.checkBoxEmployees.Name = "checkBoxEmployees";
            this.checkBoxEmployees.Size = new System.Drawing.Size(18, 17);
            this.checkBoxEmployees.TabIndex = 3;
            this.checkBoxEmployees.UseVisualStyleBackColor = true;
            // 
            // checkBoxCustomers
            // 
            this.checkBoxCustomers.AutoSize = true;
            this.checkBoxCustomers.Location = new System.Drawing.Point(208, 487);
            this.checkBoxCustomers.Name = "checkBoxCustomers";
            this.checkBoxCustomers.Size = new System.Drawing.Size(18, 17);
            this.checkBoxCustomers.TabIndex = 4;
            this.checkBoxCustomers.UseVisualStyleBackColor = true;
            // 
            // checkBoxShippers
            // 
            this.checkBoxShippers.AutoSize = true;
            this.checkBoxShippers.Location = new System.Drawing.Point(208, 535);
            this.checkBoxShippers.Name = "checkBoxShippers";
            this.checkBoxShippers.Size = new System.Drawing.Size(18, 17);
            this.checkBoxShippers.TabIndex = 5;
            this.checkBoxShippers.UseVisualStyleBackColor = true;
            // 
            // lblEmployees
            // 
            this.lblEmployees.AutoSize = true;
            this.lblEmployees.Location = new System.Drawing.Point(245, 438);
            this.lblEmployees.Name = "lblEmployees";
            this.lblEmployees.Size = new System.Drawing.Size(81, 17);
            this.lblEmployees.TabIndex = 6;
            this.lblEmployees.Text = "Employees:";
            // 
            // lblCustomers
            // 
            this.lblCustomers.AutoSize = true;
            this.lblCustomers.Location = new System.Drawing.Point(245, 487);
            this.lblCustomers.Name = "lblCustomers";
            this.lblCustomers.Size = new System.Drawing.Size(79, 17);
            this.lblCustomers.TabIndex = 7;
            this.lblCustomers.Text = "Customers:";
            // 
            // lblShippers
            // 
            this.lblShippers.AutoSize = true;
            this.lblShippers.Location = new System.Drawing.Point(245, 538);
            this.lblShippers.Name = "lblShippers";
            this.lblShippers.Size = new System.Drawing.Size(68, 17);
            this.lblShippers.TabIndex = 8;
            this.lblShippers.Text = "Shippers:";
            // 
            // cboEmployees
            // 
            this.cboEmployees.FormattingEnabled = true;
            this.cboEmployees.Location = new System.Drawing.Point(341, 435);
            this.cboEmployees.Name = "cboEmployees";
            this.cboEmployees.Size = new System.Drawing.Size(121, 24);
            this.cboEmployees.TabIndex = 9;
            // 
            // cboCustomers
            // 
            this.cboCustomers.FormattingEnabled = true;
            this.cboCustomers.Location = new System.Drawing.Point(341, 484);
            this.cboCustomers.Name = "cboCustomers";
            this.cboCustomers.Size = new System.Drawing.Size(121, 24);
            this.cboCustomers.TabIndex = 10;
            // 
            // cboShippers
            // 
            this.cboShippers.FormattingEnabled = true;
            this.cboShippers.Location = new System.Drawing.Point(341, 535);
            this.cboShippers.Name = "cboShippers";
            this.cboShippers.Size = new System.Drawing.Size(121, 24);
            this.cboShippers.TabIndex = 11;
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(682, 431);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(91, 28);
            this.buttonShow.TabIndex = 12;
            this.buttonShow.Text = "Show";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 598);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.cboShippers);
            this.Controls.Add(this.cboCustomers);
            this.Controls.Add(this.cboEmployees);
            this.Controls.Add(this.lblShippers);
            this.Controls.Add(this.lblCustomers);
            this.Controls.Add(this.lblEmployees);
            this.Controls.Add(this.checkBoxShippers);
            this.Controls.Add(this.checkBoxCustomers);
            this.Controls.Add(this.checkBoxEmployees);
            this.Controls.Add(this.lblOrders);
            this.Controls.Add(this.dataGridViewOrders);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewOrders;
        private System.Windows.Forms.Label lblOrders;
        private System.Windows.Forms.CheckBox checkBoxEmployees;
        private System.Windows.Forms.CheckBox checkBoxCustomers;
        private System.Windows.Forms.CheckBox checkBoxShippers;
        private System.Windows.Forms.Label lblEmployees;
        private System.Windows.Forms.Label lblCustomers;
        private System.Windows.Forms.Label lblShippers;
        private System.Windows.Forms.ComboBox cboEmployees;
        private System.Windows.Forms.ComboBox cboCustomers;
        private System.Windows.Forms.ComboBox cboShippers;
        private System.Windows.Forms.Button buttonShow;
    }
}

