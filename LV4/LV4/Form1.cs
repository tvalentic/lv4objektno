﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.OleDb;

namespace LV4
{
    public partial class Form1 : Form
    {
        OleDbConnection myConnection;
        OleDbDataAdapter myOleDbDataAdapter;

        BindingSource bsShippers = new BindingSource();
        BindingSource bsCustomers = new BindingSource();
        BindingSource bsEmployees = new BindingSource();
        BindingSource bsOrders = new BindingSource();


        private string myConnectionString = ConfigurationManager.ConnectionStrings["LV4"].ConnectionString;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();
                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {

                    DataSet myDataSet = new DataSet();


                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM ORDERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Orders");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SHIPPERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Shippers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM EMPLOYEES", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Employees");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CUSTOMERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Customers");

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    bsCustomers.DataSource = myDataSet;
                    bsCustomers.DataMember = "Customers";

                    bsEmployees.DataSource = myDataSet;
                    bsEmployees.DataMember = "Employees";

                    bsShippers.DataSource = myDataSet;
                    bsShippers.DataMember = "Shippers";

                    dataGridViewOrders.DataSource = bsOrders;

                    cboShippers.DataSource = bsShippers;
                    cboShippers.DisplayMember = "CompanyName";
                    cboShippers.ValueMember = "ShipperID";

                    cboEmployees.DataSource = bsEmployees;
                    cboEmployees.DisplayMember = "LastName";
                    cboEmployees.DisplayMember = "FirstName";
                    cboEmployees.ValueMember = "EmployeeID";

                    cboCustomers.DataSource = bsCustomers;
                    cboCustomers.DisplayMember = "CompanyName";
                    cboCustomers.ValueMember = "CustomerID";
                }
                myConnection.Close();
            }
        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            bool Shippers = checkBoxShippers.Checked;
            bool Employees = checkBoxEmployees.Checked;
            bool Customers = checkBoxCustomers.Checked;

            using (myConnection = new OleDbConnection(myConnectionString)) //povezivanje s bazom podataka
            {
                myConnection.Open();

                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    try
                    {
                        string mySQLString = "SELECT * FROM Orders WHERE 1=1";

                        if (Shippers)
                        {
                            mySQLString += " AND ShipVia=@ShipperID";
                        }

                        if (Employees)
                        {
                            mySQLString += " AND EmployeeID=@EmployeeID";
                        }

                        if (Customers)
                        {
                            mySQLString += " AND CustomerID=@CustomerID";
                        }

                        OleDbCommand myCommand = new OleDbCommand(mySQLString, myConnection);

                        //dodavanje parametara
                        if (Shippers) myCommand.Parameters.AddWithValue("@Shipper", cboShippers.SelectedValue.ToString());
                        if (Employees) myCommand.Parameters.AddWithValue("@EmployeeID", cboEmployees.SelectedValue.ToString());
                        if (Customers) myCommand.Parameters.AddWithValue("@CustomerID", cboCustomers.SelectedValue.ToString());
                       
                        DataSet myDataSet = new DataSet();
                        myOleDbDataAdapter.SelectCommand = myCommand;
                        myOleDbDataAdapter.Fill(myDataSet, "Orders");

                        bsOrders.DataSource = myDataSet;
                        bsOrders.DataMember = "Orders";
                        dataGridViewOrders.DataSource = bsOrders;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }
                    myConnection.Close();
                }
            }

        }
    }
}